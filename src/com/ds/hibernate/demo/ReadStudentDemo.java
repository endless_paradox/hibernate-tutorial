package com.ds.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Student;

public class ReadStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create a session
		Session session = factory.getCurrentSession();
		
		try	{
			//use session to save the java object
			
			//create a student object
			System.out.println("Create a new student object...");
			Student theStudent = new Student("Daffy", "Duck", "daffy@xyz.com");
			
			//start a transaction
			session.beginTransaction();
			
			// save the student object
			System.out.println("saving the student");
			System.out.println(theStudent);
			session.save(theStudent);
			
			// commit transaction
			session.getTransaction().commit();
			
			// MY new Code
			
			// find out the id
			System.out.println("Generated ID:" + theStudent.getId());
			
			// Now get a new session and start the transaction
			session = factory.getCurrentSession();
			session.beginTransaction();
			
			// Retrieve student based on the id
			System.out.println("\nGetting student with the id: " + theStudent.getId());
			
			Student myStudent = session.get(Student.class, theStudent.getId());
			
			System.out.println("Get Complete: "+myStudent);
			// commit the transaction 
			session.getTransaction().commit();
			System.out.println("done");
		}
		finally {
			factory.close();
		}
		
	}

}
