package com.ds.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Instructor;
import com.ds.hibernate.demo.entity.InstructorDetail;

public class DeleteDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		//create a session
		Session session = factory.getCurrentSession();
		
		try	{
			//use session to save the java object
			
			//start a transaction
			session.beginTransaction();
			
			int theId = 1;
			
			Instructor theInstructor = session.get(Instructor.class, theId);
			
			if(theInstructor != null)	{
				System.out.println("Deleting: "+theInstructor);
				session.delete(theInstructor);
			}
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("done");
		}
		finally {
			factory.close();
		}
		
	}

}
