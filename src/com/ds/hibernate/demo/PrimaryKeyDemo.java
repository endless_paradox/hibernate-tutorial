package com.ds.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Student;

public class PrimaryKeyDemo {

	public static void main(String[] args) {
		
		// create session factory
				SessionFactory factory = new Configuration()
										.configure("hibernate.cfg.xml")
										.addAnnotatedClass(Student.class)
										.buildSessionFactory();
				
				//create a session
				Session session = factory.getCurrentSession();
				
				try	{
					//use session to save the java object
					
					//create 3 student object
					System.out.println("Create 3 student object...");
					Student theStudent = new Student("Paul", "Wall", "paul@xyz.com");
					Student theStudent2 = new Student("John", "Doe", "john@xyz.com");
					Student theStudent3 = new Student("Bonita", "Apple", "bonita@xyz.com");
					
					//start a transaction
					session.beginTransaction();
					
					// save the student object
					System.out.println("saving the student");
					session.save(theStudent);
					session.save(theStudent2);
					session.save(theStudent3  );
					
					// commit transaction
					session.getTransaction().commit();
					
					System.out.println("done");
				}
				finally {
					factory.close();
				}

	}

}
