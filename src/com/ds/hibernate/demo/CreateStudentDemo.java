package com.ds.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Student;

public class CreateStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create a session
		Session session = factory.getCurrentSession();
		
		try	{
			//use session to save the java object
			
			//create a student object
			System.out.println("Create a new student object...");
			Student theStudent = new Student("Paul", "Wall", "paul@xyz.com");
			
			//start a transaction
			session.beginTransaction();
			
			// save the student object
			System.out.println("saving the student");
			session.save(theStudent);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("done");
		}
		finally {
			factory.close();
		}
		
	}

}
