package com.ds.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Student;

public class QueryStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create a session
		Session session = factory.getCurrentSession();
		
		try	{

			
			//start a transaction
			session.beginTransaction();
			
			//query the students
			List<Student> theStudents = session.createQuery("from Student").getResultList();
			
			//display the students
			displayStudents(theStudents);
			
			//query students: lastName="doe"
			theStudents = session.createQuery("from Student s where s.lastName= 'doe'").getResultList();
			
			// display the students
			System.out.println("\n\n\nstudent whose last name is doe");
			displayStudents(theStudents);
			
			// query students: latName: doe or firstName: daffy
			theStudents = session.createQuery("from Student s where lastName='doe'"
					+ " OR firstName='daffy'").getResultList();
			
			// display the students
			System.out.println("\n\n\nstudent whose last name is doe and firstName is daffy");
			displayStudents(theStudents);
			
			// query student using the Like query
			
			theStudents = session.createQuery("from Student s where s.email LIKE '%xyz.com'").getResultList();
			
			// display the students
			System.out.println("\n\n\nstudent whose email like xyz.com");
			displayStudents(theStudents);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("done");
		}
		finally {
			factory.close();
		}
		
	}

	private static void displayStudents(List<Student> theStudents) {
		for(Student tempStudent: theStudents)	{
			System.out.println(tempStudent);
		}
	}

}
