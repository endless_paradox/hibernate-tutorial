package com.ds.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Student;

public class DeleteStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create a session
		Session session = factory.getCurrentSession();
		
		try	{
			
			int studentId = 6;
			//start a transaction
			session.beginTransaction();
			
//			Student myStudent = session.get(Student.class, studentId);
//			
//			System.out.println("Deleting Student: " + myStudent);
//			session.delete(myStudent);
			
			// delete student with student id = 2
			
			session.createQuery("delete Student where id = 7").executeUpdate();
			
			session.getTransaction().commit();
			// commit transaction
			System.out.println("done");
		}
		finally {
			factory.close();
		}
		
	}

}
