package com.ds.hibernate.demo;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Employee;

public class CreateEmployeeDemo {
	public static void main(String[] args)	{
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Employee.class)
								.buildSessionFactory();
		
		Session session = factory.getCurrentSession();
		
		try	{
			Employee tempEmployee = new Employee("Paul", "Wall", "Capg");
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			session.save(tempEmployee);
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			Employee myEmployee = session.get(Employee.class, tempEmployee.getId());
			
			System.out.println("\n\n\n" + myEmployee);
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			List<Employee> myEmployees = session.createQuery("from Employee e where e.Company = 'capg'").getResultList();
			
			System.out.println(myEmployees);
			System.out.println("\n\n\n\n");
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			Employee theEmployee = session.get(Employee.class, tempEmployee.getId());
			
			session.delete(theEmployee);
			
			session.getTransaction().commit();
			
			System.out.println("done");
			
		}
		finally	{
			factory.close();
		}
	}
}
