package com.ds.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Student;

public class UpdateStudentDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create a session
		Session session = factory.getCurrentSession();
		
		try	{
			//use session to save the java object
			
			//start a transaction
			session.beginTransaction();
			
			// Update a student information
			
			int studentId = 6;
			
			Student myStudent = session.get(Student.class, studentId);
			
			myStudent.setFirstName("Scooby");
			
			// commit transaction
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			//get the details from database
			
			Student theStudent = session.get(Student.class, studentId);
			
			System.out.println("\n" + theStudent);
			
			session.getTransaction().commit();
			
			session = factory.getCurrentSession();
			
			session.beginTransaction();
			
			// update all student email address
			
			session.createQuery("Update Student set email='foo@abc.com'").executeUpdate();
			
			session.getTransaction().commit();
			
			System.out.println("done");
		}
		finally {
			factory.close();
		}
		
	}

}
