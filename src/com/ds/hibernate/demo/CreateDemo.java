package com.ds.hibernate.demo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.ds.hibernate.demo.entity.Instructor;
import com.ds.hibernate.demo.entity.InstructorDetail;

public class CreateDemo {

	public static void main(String[] args) {

		// create session factory
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.buildSessionFactory();
		
		//create a session
		Session session = factory.getCurrentSession();
		
		try	{
			//use session to save the java object
			
			//create a object
			System.out.println("Create a new Instructor object...");
//			Instructor tempInstructor = new Instructor("chad", "darby","darby@xyz.com");
//			InstructorDetail tempInstructorDetail = new InstructorDetail("www.youtube.com/carry", "roast");
			
			Instructor tempInstructor = new Instructor("Madhu", "Patel","madhu@xyz.com");
			InstructorDetail tempInstructorDetail = new InstructorDetail("www.youtube.com/madhu", "code");
			
			// associate the objects
			tempInstructor.setInstructorDetail(tempInstructorDetail);
			
			//start a transaction
			session.beginTransaction();
			
			// save the instructor
			System.out.println("saving instructor: "+ tempInstructor);
			session.save(tempInstructor);
			
			// commit transaction
			session.getTransaction().commit();
			
			System.out.println("done");
		}
		finally {
			factory.close();
		}
		
	}

}
